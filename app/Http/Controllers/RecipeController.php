<?php

namespace App\Http\Controllers;

use App\Recipe;
use App\Type;
use Illuminate\Http\Request;
use Session;
use App\Http\Requests\RecipeStoreRequest;
use Reques;




class RecipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        /** dd($req->get('name')); para comprobar que se esta mandandando un 
         * parametro name por POST
         * busca en los scopes de su clase el Name
        */

        $req->user()->authorizeRoles(['admin','user','guess']);

        // 2 PASO de la barra filtro: en el modelo recipe se creo la funcion name, asi que 
        //la utiliza para bucar
        $myres=Recipe::Name($req->get('name'))->orderby("id","desc")->paginate(10);
        
        return view('recipes.index')->with('myres',$myres);
        //return view("recipes.index",compact("myres"));
    }

    public function indexdt(Request $req)
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $req)
    {
        $req->user()->authorizeRoles(['admin','user']);
        //$mytype=Type::orderby("type","asc")->get();
        $mytype=Type::select("id","type")->get();
        //return view("recipes.create",compact("mytype"));
        return view("recipes.create")->with("mytype",$mytype);
        //mytype,$mytype son una referencia a la variable de arriba, el primero es
        //como un alias para la segunda, solo quita el $
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RecipeStoreRequest $request)
    {
        //sintaxis para el selec de llave foranea
        //$datos=$request->all();
        //Recipe::create($datos);   
        
        //dd($request)
        //guardando control por control
        //$recipe=New Recipe;
        //$recipe->name=$request->name;
        //$recipe->ingredients=$request->ingredients;
        //$recipe->procedure=$request->procedure;
        //$recipe->type_id=$request->type_id;
        //$recipe->save();

        //guardando de un solo paso con request->all(), usado en tablas sin relacionar
        //Recipe::create($request->all());      
        

        //nuevo procedimiento que incluye el guardado de imagen
        $entrada=$request->all();

        if ($archivo=$request->file('file')){
            $nombre=$archivo->getClientOriginalName();
            $$archivo->move('public/images',$nombre);
            $entrada['route']=$nombre;
            
            
        }
        Recipe::create($entrada);

        Session::flash('message','Information saved');
        return redirect()->route('recipes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function show(Recipe $recipe)
    {
        //genera el pdf
        //$recipes=\App\Recipe::find($id);
        //$view=$view('recipes.show',compact('recipes'));
        //$pdf=\App::make('dompdf.wrapper');
        //$pdf->loadHTML($view);
        //return $pdf->stream('recipes');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function edit(Recipe $recipe, Request $req)
    {
        $req->user()->authorizeRoles(['admin','user']);
        //dd($recipe->route);
        
        return view("recipes.edit",compact('recipe'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function update(RecipeStoreRequest $request, Recipe $recipe)
    {
        /*$request->validate([
            'recipe'=>'required',
            
        ]);*/

        $recipe->update($request->all());
        Session::flash('message','Information updated');
        return redirect()->route('recipes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function destroy(Recipe $recipe, Request $req)
    {
        $req->user()->authorizeRoles(['admin','user']);

        $recipe->delete();
        Session::flash('message','Information deleted');
        return redirect()->route('recipes.index');
    }


    public function viewpdf(){
        return view('recipes.pdf');
    }


    public function filePDF(Request $req)
    {
        //$recipes=Recipe::find($r->input('id')); //busca en reques 
        //todo lo que manda el formulario, el problema es que no implica 
        //seleccionar un id inicia vacio XXX
                
        $name=$req->get('name');//busca en la ruta ?id=2, pero se pone manual OK
        
        /* dd($req->get('name'));*/

        $recipes= Recipe::where('name',$name)->get(); //OK pero estatico
        $pdf=\PDF::LoadView('rep.r1',['recipes'=>$recipes]);
        //en el video no explica bien el porque del \PDF
        return $pdf->download('r1.pdf'); 
        echo "hi pdf";
    }
}
