<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class esAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user=Auth::user();

        $vacio=$user->esAdmin();


        //if(!$user->esAdmin())
        if(is_null($vacio))
        {
            return redirect('/');
        }
        

        return $next($request);
    }
}
