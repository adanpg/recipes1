<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
		protected $fillable=['name','type_id','ingredients','procedure','route'];
	

    public function type()
		{
			return $this->belongsTo("App\Type","type_id","id");
		}

	// TERCER PASO: funcion que utiliza la barra de busqueda del index
	//prefijo scope
	//video: busquedas y filtros con laravel y eloquent (query scopes)
	//duilio palacion
	public function scopeName($query,$name){
		if(trim($name)!="")
		{
		//$query->where('name',$name);
		$query->where(\DB::raw("name"),"like","%$name%");
		//por ejemplo si se quisiera concatenar los campos, se utiliza sintaxis mysal
		//where(\DB::raw(CONCAT(name,secname),"like","%$ame%");
		
		
		}
	}
}
