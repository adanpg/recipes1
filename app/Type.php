<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Recipe;


class Type extends Model
{

	protected $fillable=['type'];

    public function recipe()
		{
			return $this->hasOne("App\Recipe");
		}
}
