<?php

namespace App;


use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Role;



class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


//se modifico belongsTo por belongsToMany
    //public function role()
    //{
    //    return $this->belongsToMany('App\Role');
    //}

    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }


//creada en la practica del video de middleware

    public function esAdmin()
    { 
        
            if(!$this->role->nombre_rol=='administrador')
            {
                return false;
            }
            else
            {
                return true;
            }
   
    }
    
 //valida si vienen uno o varios roles en un array   
    public function hasAnyRole($roles){
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }
        return false;
    }
    
//valida si el usuario tiene relacionado algun ro
    public function hasRole($role){
        if ($this->roles()->where('name', $role)->first()) {
            return true;
        }
        return false;
    }


    public function authorizeRoles($roles){
        if ($this->hasAnyRole($roles)) {
            return true;
        }
        //busca el 401 autom...
        abort(401, '');
    }

}
