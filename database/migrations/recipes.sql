-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         5.7.24 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura para tabla recipes.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla recipes.migrations: ~6 rows (aproximadamente)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_05_20_183822_create_types_table', 1),
	(4, '2019_05_21_043456_create_recipes_table', 1),
	(5, '2019_06_06_193229_create_roles_table', 1),
	(6, '2019_07_29_202457_create_role_user_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Volcando estructura para tabla recipes.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla recipes.password_resets: ~0 rows (aproximadamente)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Volcando estructura para tabla recipes.recipes
CREATE TABLE IF NOT EXISTS `recipes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ingredients` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `procedure` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0.jpg',
  `type_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `recipes_type_id_foreign` (`type_id`),
  CONSTRAINT `recipes_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla recipes.recipes: ~2 rows (aproximadamente)
DELETE FROM `recipes`;
/*!40000 ALTER TABLE `recipes` DISABLE KEYS */;
INSERT INTO `recipes` (`id`, `name`, `ingredients`, `procedure`, `route`, `type_id`, `created_at`, `updated_at`) VALUES
	(4, 'LEGUMBRES AL ESCABECHE', 'LEGUMBRES, VINAGRE AL GUSTO, LAUREL, CHILES EN VINAGRE', 'En rodajas junto con laurel se pone a freír a fuego lento hasta que cristalice la,cebolla. De preferencia los vegetales mas duros primero y al final los mas blandos. Se le pone sal al gusto.  Luego se agrega un poco de agua y otro de vinagre y se tapa para que hierva a fuego lento 10 minutos. Ya,frio se,puede agregar una lata de chiles en vinagre', '0.jpg', 4, '2019-12-27 16:00:14', '2019-12-27 16:00:14'),
	(5, 'ARROZ CON LECHE', 'MEDIO KILO DE ARROZ, 3 LITROS DE LECHE, PASAS Y CANELA AL GUSTO', 'Tres litros de leche a hervir con canela. Hirviendo se le pone medio kilo de arroz limpio sin lavar, se mueve hasta que este a punto y se deja enfriar, agregando pasas o nuez.', '0.jpg', 3, '2019-12-27 16:08:23', '2019-12-27 16:08:23');
/*!40000 ALTER TABLE `recipes` ENABLE KEYS */;

-- Volcando estructura para tabla recipes.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla recipes.roles: ~3 rows (aproximadamente)
DELETE FROM `roles`;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'have all privileges in the data base', '2019-09-30 17:02:02', '2019-09-30 17:02:02'),
	(2, 'user', 'can do administrative works', '2019-09-30 17:02:02', '2019-09-30 17:02:02'),
	(3, 'guess', 'can see some views and general information', '2019-09-30 17:02:02', '2019-09-30 17:02:02');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Volcando estructura para tabla recipes.role_user
CREATE TABLE IF NOT EXISTS `role_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla recipes.role_user: ~4 rows (aproximadamente)
DELETE FROM `role_user`;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, NULL, NULL),
	(2, 2, 2, NULL, NULL),
	(3, 3, 3, NULL, NULL),
	(4, 1, 4, NULL, NULL);
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;

-- Volcando estructura para tabla recipes.types
CREATE TABLE IF NOT EXISTS `types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla recipes.types: ~3 rows (aproximadamente)
DELETE FROM `types`;
/*!40000 ALTER TABLE `types` DISABLE KEYS */;
INSERT INTO `types` (`id`, `type`, `created_at`, `updated_at`) VALUES
	(1, 'ASADO', '2019-09-30 17:02:03', '2019-09-30 17:02:03'),
	(2, 'ADEREZO', '2019-09-30 17:02:03', '2019-09-30 17:02:03'),
	(3, 'POSTRE', '2019-09-30 17:02:03', '2019-09-30 17:02:03'),
	(4, 'COMPLEMENTO', NULL, NULL);
/*!40000 ALTER TABLE `types` ENABLE KEYS */;

-- Volcando estructura para tabla recipes.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla recipes.users: ~3 rows (aproximadamente)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'admin@example.com', NULL, '$2y$10$jZzLK21ie3COY4PrGd/hPOOAZZNLHP/dOUenz0WLglYDaAT0Z0bmy', NULL, '2019-09-30 17:02:02', '2019-09-30 17:02:02'),
	(2, 'user', 'user@example.com', NULL, '$2y$10$UK39e6XkfHXt9ku6yvqnQuMmO7MaaADdbiLAyZN5SIimvsv4.WSzm', NULL, '2019-09-30 17:02:03', '2019-09-30 17:02:03'),
	(3, 'guess', 'guess@example.com', NULL, '$2y$10$.v1rxuZYgNJbA4LHbS/BC.Eyom8l7lIaEyKZhbZWnWO3HnVLLd2My', NULL, '2019-09-30 17:02:03', '2019-09-30 17:02:03'),
	(4, 'adan', 'adan@mail.com', NULL, '$2y$10$7HBfcx6NxZ4GMEA.fXouk.VIjxyJOHFiFwHgFbqjMVfsQms/DEcia', NULL, '2019-09-30 17:03:48', '2019-09-30 17:03:48');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
