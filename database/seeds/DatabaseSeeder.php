<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
        // La creación de datos de roles debe ejecutarse primero
        $this->call(RoleTableSeeder::class);

        // Los usuarios necesitarán los roles previamente generados
        $this->call(UserTableSeeder::class);

        $this->call(TypeTableSeeder::class);




        //tablas que requieren backup ***********
        //el seed funcionara sobre el backup que se creo manualmente
        //desde la interface. en la consola no corre el mysqldump
        //luego con php artisan migrate:fresh y DB:seed
        $recipes='database/migrations/recipes.sql';
        DB::unprepared(file_get_contents($recipes));

       /*  $role_user='database/migrations/role_user.sql';
        DB::unprepared(file_get_contents($role_user));      

        $users='database/migrations/users.sql';
        DB::unprepared(file_get_contents($users));



        //tablas auxiliares *********************
        $roles='database/migrations/roles.sql';
        DB::unprepared(file_get_contents($roles));

        $types='database/migrations/types.sql';
        DB::unprepared(file_get_contents($types)); */
        
    }
}
