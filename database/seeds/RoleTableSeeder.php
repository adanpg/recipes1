<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->name = 'admin';
        $role->description = 'have all privileges in the data base';
        $role->save();

        $role = new Role();
        $role->name = 'user';
        $role->description = 'can do administrative works';
        $role->save();

        $role = new Role();
        $role->name = 'guess';
        $role->description = 'can see some views and general information';
        $role->save();
    }
}
