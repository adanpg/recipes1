<?php

use Illuminate\Database\Seeder;
use App\Type;

class TypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = new Type();
        $type->type = 'ASADO';
        $type->save();

        $type = new Type();
        $type->type = 'ADEREZO';
        $type->save();

        $type = new Type();
        $type->type = 'POSTRE';
        $type->save();
    }
}


