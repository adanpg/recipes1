<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <style>
    h1{
        font-weight: bold;
        color:brown;
        text-size-adjust:60px;
        padding-top: 150px;
        width: 100%;
        text-align: center;
    }

    h2,h3,h4{
        width: 100%;
        color:brown;
        text-align: center;
    }

    a{
        width: 100%;
        color:brown;
        text-align: center;
    }
    </style>

<div>
    <h1>Error 401</h1>
    <h2>This action is Unauthorized for current user</h2>
    <h3>You must be logued with correct privileges</h3>
    <h4><a href="{{url('/')}}">Login</a></h4>

</div>

    
</body>
</html>