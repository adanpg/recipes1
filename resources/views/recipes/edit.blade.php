@extends('recipes.template')

@section('content')

<div class="container">

<h3>Edit Recipe</h3>

<!--el post llama a la vista update y le pasa el id de recipe
mas abajo especifica que es con el metodo PUT-->
<form action="{{route('recipes.update',$recipe->id)}}" method="POST">
@csrf
@method('PUT')

<style>
#image{
    width:200px;
    height:220px
}
</style>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <strong>Name</strong>
            <input type="text" name="name" value="{{$recipe->name}}" 
            class="form-control" placeholder="recipe's name" style='text-transform:uppercase'>

            <strong>INGREDIENTS</strong>
            <input type="text" name="ingredients" value="{{$recipe->ingredients}}" 
            class="form-control" placeholder="ingredients" style='text-transform:uppercase'>

            <strong>PROCEDURE</strong>
            <input type="text" name="procedure" value="{{$recipe->procedure}}" 
            class="form-control" placeholder="procedure" style='text-transform:uppercase'>

            <!--no funciono con el codigo de pildoras-->
            <strong>IMAGE</strong>
            <img id='image' name="image" src="{{asset('images/'.$recipe->route)}}" class="form-control"/>
            
            <!--cacha error-->
            @if($errors->has('name'))
                <strong class="text-danger">{{$errors->first('name')}}</strong>
            @endif
        </div>
    </div>
    <!--
    <div class="col-md-12">
        <div class="form-group">
            <strong>Burn</strong>
            <input type="text" name="burn" value="{{$recipe->burn}}" class="form-control" placeholder="fecha de  nacimiento" >
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <strong>Notes</strong>
            <input type="text" name="notes" value="{{$recipe->notes}}" class="form-control" placeholder="notas" >
        </div>
    </div> -->
    <div class="col-md-12 text-center" >
        <button type="submit" class="btn btn-primary">Update</button>
    </div>
    
</div>

</form>

</div>


@endsection


