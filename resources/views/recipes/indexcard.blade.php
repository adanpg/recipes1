<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>


<style>
*{
  margin=0;
  padding=0;
  box-sizing:border-box;
}

.wrap{
  width:1100px;  
  margin:40px auto;

  display: flex;
  justify-content:center;  
}

.tarjeta-wrap{
 margin: 10px; 



-webkit-perspective:800;
perspective:800;

}

.tarjeta{
  background-color:orange;

  width:250px;
  height:300px;
  position:relative;
  
  transform-style:preserve-3d;
  transition: 0.7s ease;

  box-shadow: 2px 2px 10px green; 
}

.delante, .detras{
  height:100%;
  width:100%;
  
  position:absolute;

  top:0;
  left:0;

  backface-visibility:hidden;
  -webkit-backface-visibility:hidden;

  justify-content:center;
}

.detras{
  transform: rotateY(180deg);
}

.tarjeta-wrap:hover .tarjeta{
  transform: rotateY(180deg);
}




/*------------------------------------------------------------------
--------------------------------------------------------------------
*/
#name{
  
  font-size: 12px;
  text-align: center;

  
}

#descrip{
  
  font-size: 9px;
  text-align: center;

  
}

#image{
  width:250px;
  height:300;
}




</style>

<body>

<div class="wrap" >

  @foreach($recipes as $rec)
  <div class="tarjeta-wrap">
    <div class="tarjeta">
      <div class="delante">
        <img id="image" src="{{asset('images/'.$rec->route)}}"/><br>
        <strong id="name">{{$rec->name}}</strong> <br>
        
      </div>
      <div class="detras">
        <strong id="descrip">{{$rec->ingredients}}</strong> <br><br>
        <strong id="descrip">{{$rec->procedure}}</strong>
      </div>
    <!--del video : https://www.youtube.com/watch?v=5mrkxGPzyK0
    tarjeta 3d con efecto flip-->  
    </div>
    
  </div>

  @endforeach
</div>

</body>
</html>

