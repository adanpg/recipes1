<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>


<!--hojas de estilos de datatable.net-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">


<body>
  


<div id="container">

<!--segun el video de rimorsoft online tip 3 y 4, data tables y ajax en laravel
data tables
-->
<table id="recipesdt" class="table">
<thead>
  <tr>
    <th>ID</th>
    <th>NAME</th>
    <th>INGREDIENTS</th>
    <th>PROCEDURE</th>
    
    
  </tr>
</thead>
<tbody>
@foreach($myres as $mr)
  <tr>
    <td>{{$mr->id}}</td>
    <td>{{$mr->name}}</td>
    <td>{{$mr->ingredients}}</td>
    <td>{{$mr->procedure}}</td>
    
  </tr>
@endforeach
</tbody>

</table>




<!--los scripts de datatable.net-->
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

<script>
$(document).ready(function() {
$('#recipesdt').DataTable();
});
</script>

</div>



</body>
</html>

