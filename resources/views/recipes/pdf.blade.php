@extends('recipes.template')

@section('content')

<h3>Reports</h3>

 
<!--
<form method="POST">
<a class="nav-link" href="{{action('RecipeController@filePDF')}}">Report</a>
</form>
-->

<form action="{{action('RecipeController@filePDF')}}" method="GET">

<strong>Name</strong>
<input type="text" value="{{old('name')}}" name="name" class="form-control" 
     placeholder="recipe's name" style='text-transform:uppercase'>
    <button type="submit"  class="btn-sm btn-info">Ver PDF</button>
</form>

@endsection