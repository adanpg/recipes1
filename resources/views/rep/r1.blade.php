<html>
 <style>
 table {
   width: 100%;
   border: 1px solid #000;
}
th, td {
   width: 25%;
   text-align: left;
   vertical-align: top;
   border: 1px solid #000;
   border-collapse: collapse;
   padding: 0.3em;
   caption-side: bottom;
}
caption {
   padding: 0.3em;
   color: #fff;
    background: #000;
}
th {
   background: #eee;
}
 </style>



<table>
    <head>
        <tr>
        <th>NAME</th>
        <th>INGREDIENTS</th>
        </tr>
    </head>
    <body>
        
        @foreach($recipes as $r)
        <tr>
            <td>{{$r->name}}</td>
            <td>{{$r->ingredients}}</td>
        </tr>
        @endforeach
        
    </body>

</table>
</html>