@extends('recipes.template')

@section('content')

<div class="container">

<h3>Edit Recipe</h3>

<!--el post llama a la vista update y le pasa el id de recipe
mas abajo especifica que es con el metodo PUT-->
<form action="{{route('users.update',$user->id)}}" method="POST">
@csrf
@method('PUT')

<style>
#image{
    width:200px;
    height:220px
}
</style>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <strong>Name</strong>
            <input type="text" name="name" value="{{$user->name}}" 
            class="form-control" placeholder="user's name" style='text-transform:uppercase'>
           
            
            <div class="form-group">
            <strong>{!! Form::label("id","New role")!!}</strong>
            </div>

            {!!Form::select('idrole',$roles->pluck('name','id')->all(),
            null,['placeholder'=>'--Select role--','class'=>'form-control'])!!}         
           
            
            <!--cacha error-->
            @if($errors->has('name'))
                <strong class="text-danger">{{$errors->first('name')}}</strong>
            @endif
        </div>
    </div>
    
    <div class="col-md-12 text-center" >
        <button type="submit" class="btn btn-primary">Update</button>
    </div>
    
</div>

</form>

</div>


@endsection


