<?php
use App\Type;
use App\Recipe;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/recipes/busca', function () {

    //pruebas para mostrar los datos relacionados
    //return Type::find(1)->recipe; 
    return Type::pluck("id","type");
});

//prueba para mostrar el id de un select al seleccionarlo
Route::get('/recipes/pruebas', function () {
    return view("/recipes/pruebas");
    
});

Route::get('/', function() 
{
    /*user=Auth::user();

    if($user->esAdmin())
    {
        echo 'Administrador';
    }
    else
    {
        echo 'Otro';
    };
*/ 
    //para que inicie con la vista de autenticacion
    return view("auth/login");
    
})->name('login');

//como en  las rutas automaticas toma la carpeta inicial recipes, en la ruta
//luego de public va recipes. y las diferentes views
Route::resource('recipes','RecipeController');

Auth::routes();

Route::get('home', 'HomeController@index')->name('home');

//debe validar si es administrador para entrar al admin
Route::get('/admin','AdministradorController@Index');

//Route::get('/index_ru','UserController@index_ru');

Route::resource('users','UserController');
//Route::POST('/index_ru/destroy','UserController@destroy');




Route::get('/pdf', 'RecipeController@viewpdf');
   
   //$recipes= Recipe::all();
   //$pdf=PDF::LoadView('rep.r1',['recipes'=>$recipes]);
   //return $pdf->download('r1.pdf'); 
//});

/*
esta ruta se declaro para que se pueda invocar la funcion filePDF
duda: porque no pide ruta declarada para la otra funcion creada que
fue viewpdf. dice la ayuda que se requieren rutas para cualquier metodo
que se invoque desde formularios o enlaces
llamado desde un formulario con GET: <form action="{{action('RecipeController@filePDF')}}" method="GET">
*/
Route::GET('/filePDF', 'RecipeController@filePDF');
//Route::get('/filePDF', array('uses' => 'RecipeController@filePDF'));


/* <a href="{{url('recipes.indexdt')}}">Recipes DT</a> 
la duda es que no llama a la view por medio del controller, por lo 
tanto no le pasa los datos para llenar la tabla, y aun asi al abrir
sale la tabla llena
*/

/* llamar directo a la view para el datatable 
en primera instancia se aprecia que no deberia armarse data desde aqui
sino en el controlador

*/
Route::get('/indexdt', function(){
    $myres=Recipe::all();
    return view('recipes.indexdt')->with('myres',$myres);
});

Route::get('/indexcard', function(){
    $recipes=Recipe::orderby('id','desc')->take(3)->get();
    return view('recipes.indexcard')->with('recipes',$recipes);
});